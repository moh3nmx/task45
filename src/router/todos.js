import Layout from '../layouts/Main.vue'
import Todos from '../views/Todos.vue'
export default [
  {
    path: '/todo-list',
    name: 'Todos',
    component: Layout,
    children: [
      {
        path: '',
        name: 'TodosList',
        component: Todos
      }
    ]
  }
]