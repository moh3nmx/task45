import Vue from 'vue'
import VueRouter from 'vue-router'

// import other routes
import todos from './todos'

// import view components
import Home from '../views/Home.vue'
import Layout from '../layouts/Main.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Layout,
    children: [
      {
        path: '',
        name: 'Home',
        component: Home
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...routes,
    ...todos
  ]
})

export default router
