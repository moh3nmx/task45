import axios from 'axios'
import { todos } from './endpoints'
export default {
  getTypicodeList(){
    return axios(todos.list)
  }
}