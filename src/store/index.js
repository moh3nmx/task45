import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})
const cardTemplate = () => {
  return {
    description: '',
    created: new Date(),
    updated: new Date()
  }
}
export default new Vuex.Store({
  state: {
    lists: [],
  },
  mutations: {
    unshiftList(state, newList){
      if(state.lists.length && state.lists[0].static) state.lists.splice(0, 1) 
      state.lists.unshift(newList)
    },
    newList(state, title){
      state.lists.push({
        title,
        cards: []
      })
    },
    removeList(state, index){
      state.lists.splice(index, 1)
    },
    editListTitle(state, { index, title }){
      state.lists[index].title = title
    },
    addNewCard(state, {index, title}){
      state.lists[index].cards.unshift({
        title,
        ...cardTemplate()
      })
    },
    updateCard(state, { index, tIndex, title, description }) {
      state.lists[index].cards[tIndex].title = title
      state.lists[index].cards[tIndex].description = description
      state.lists[index].cards[tIndex].updated = new Date()
    },
    removeCard(state, { index, tIndex }) {
      state.lists[index].cards.splice(tIndex, 1)
    },
    changeCardList(state, {index, tIndex, newList}) {
      let selectedCard = state.lists[index].cards[tIndex]
      selectedCard.updated = new Date()
      state.lists[newList].cards.unshift(selectedCard)
      state.lists[index].cards.splice(tIndex, 1)
    }
  },
  actions: {
  },
  getters: {
    lists: (state) => state.lists
  },
  modules: {
  },
  plugins: [vuexLocal.plugin]
})
